# Extended renovate bot image

This image extends the renovate bot image with tools used by post upgrade comands.

- renovate bot itself
- curl
- jq
- yq
- msort
